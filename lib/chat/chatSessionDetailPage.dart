import 'package:flutter/material.dart';
import 'package:flutterholmuskapp/service/chat/data/chatData.dart';

class ChatSessionDetailPage extends StatefulWidget {
  final ChatSessionMeta _chatSessionMeta;

  ChatSessionDetailPage(@required this._chatSessionMeta);

  @override
  ChatSessionDetailPageState createState() => ChatSessionDetailPageState();
}

class ChatSessionDetailPageState extends State<ChatSessionDetailPage> {
  @override
  Widget build(BuildContext context) {
    final _chatSessionMeta = widget._chatSessionMeta;
    return Scaffold(
      appBar: AppBar(
        title: Text('Chat Session Id  ${_chatSessionMeta.id}'),
      ),
      body: Center(child: Text('${_chatSessionMeta.description}')),
    );
  }
}
