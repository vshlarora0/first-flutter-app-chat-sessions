import 'package:flutter/material.dart';
import 'package:flutterholmuskapp/chat/chatSessionDetailPage.dart';
import 'package:flutterholmuskapp/repository/chat/chatRepository.dart';
import 'package:flutterholmuskapp/service/chat/api/chatService.dart';
import 'package:flutterholmuskapp/service/chat/data/chatData.dart';

class ChatSessionsPageState extends State<ChatSessionsPage> {
  IChatRepository _chatRepository;
  Future<List<ChatSessionMeta>> _futureChatSessions;

  final List<ChatSessionMeta> _chatSessions = <ChatSessionMeta>[];
  final TextStyle _biggerFont = TextStyle(fontSize: 18.0);

  @override
  void initState() {
    _chatRepository =
        MockChatRepository(MockChatService(), MockChatDetailService());
    _futureChatSessions = _chatRepository.getAllChatSessions();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Chat Sessions'),
      ),
      body: Center(
        child: FutureBuilder<List<ChatSessionMeta>>(
          future: _futureChatSessions,
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              _chatSessions.addAll(snapshot.data);
              return _buildSuggestions();
            } else if (snapshot.hasError) {
              return Text("${snapshot.error}");
            }

            // By default, show a loading spinner.
            return CircularProgressIndicator();
          },
        ),
      ),
    );
  }

  Widget _buildSuggestions() {
    return ListView.builder(
        itemCount: (2 * _chatSessions.length) - 1,
        padding: const EdgeInsets.all(16.0),
        itemBuilder: (context, i) {
          if (i.isOdd) return Divider();
          return _buildRow(_chatSessions[i ~/ 2]);
        });
  }

  Widget _buildRow(ChatSessionMeta chatSessionMeta) {
    return ListTile(
      title: Text(
        chatSessionMeta.description,
        style: _biggerFont,
      ),
      onTap: () {
        _openChatSession(context, chatSessionMeta);
      },
    );
  }

  _openChatSession(BuildContext context, ChatSessionMeta chatSessionMeta) {
    Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => ChatSessionDetailPage(chatSessionMeta),
        ));
  }
}

class ChatSessionsPage extends StatefulWidget {
  @override
  ChatSessionsPageState createState() => ChatSessionsPageState();
}
