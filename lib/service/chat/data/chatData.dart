class ChatSessionMeta {
  final int id;
  final String title;
  final String description;

  ChatSessionMeta(this.id, this.title, this.description);
}

class ChatSessionDetail {
  final int id;
  final List<String> messages;

  ChatSessionDetail(this.id, this.messages);
}
