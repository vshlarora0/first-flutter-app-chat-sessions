import 'dart:async';

import 'package:flutter/foundation.dart';
import 'package:flutterholmuskapp/service/chat/data/chatData.dart';

class IChatService {
  Future<List<ChatSessionMeta>> fetchAllChatSessions() {}
}

class IChatDetailService {
  Future<ChatSessionDetail> fetchChatSessionDetail(int chatSessionId) {}
}

class MockChatService implements IChatService {
  @override
  Future<List<ChatSessionMeta>> fetchAllChatSessions() {
    final _mockChats = new List<ChatSessionMeta>();
    _mockChats.add(new ChatSessionMeta(1, "First", "Hello from first chat"));
    _mockChats.add(new ChatSessionMeta(2, "Second", "Hello from second chat"));
    _mockChats.add(new ChatSessionMeta(3, "Third", "Hello from third chat"));
    return new SynchronousFuture<List<ChatSessionMeta>>(_mockChats);
  }
}

class MockChatDetailService implements IChatDetailService {
  @override
  Future<ChatSessionDetail> fetchChatSessionDetail(int chatSessionId) {
    final _mockChatMessages = new List<String>();
    _mockChatMessages.add("Hello");
    _mockChatMessages.add("value");
    final _mockChatDetail = new ChatSessionDetail(chatSessionId, _mockChatMessages);
    return new SynchronousFuture<ChatSessionDetail>(_mockChatDetail);
  }
}
