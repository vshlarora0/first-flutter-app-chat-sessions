import 'package:shared_preferences/shared_preferences.dart';

class IUserRepository {
  void login(String user) {}

  void logout() {}

  bool isLoggedIn() {}

  String getLoggedInUser() {}
}

class UserRepository implements IUserRepository {
  final String _userKey = 'user_name';
  final SharedPreferences _sharedPreferences;

  UserRepository(this._sharedPreferences);

  @override
  String getLoggedInUser() => _sharedPreferences.getString(_userKey);

  @override
  bool isLoggedIn() => _sharedPreferences.getString(_userKey) != null;

  @override
  void login(String user) => _sharedPreferences.setString(_userKey, user);

  @override
  void logout() => _sharedPreferences.setString(_userKey, null);
}

class MockUserRepository implements IUserRepository {
  @override
  void login(String user) {}

  @override
  void logout() {}

  @override
  String getLoggedInUser() {}

  @override
  bool isLoggedIn() {}
}
