import 'package:flutterholmuskapp/service/chat/api/chatService.dart';
import 'package:flutterholmuskapp/service/chat/data/chatData.dart';

class IChatRepository {
  Future<List<ChatSessionMeta>> getAllChatSessions() {}

  Future<ChatSessionDetail> getChatSessionDetail(int chatSessionId) {}
}

class MockChatRepository implements IChatRepository {
  final IChatService _chatService;
  final IChatDetailService _chatDetailService;

  MockChatRepository(this._chatService, this._chatDetailService);

  @override
  Future<List<ChatSessionMeta>> getAllChatSessions() async {
    return await _chatService.fetchAllChatSessions();
  }

  @override
  Future<ChatSessionDetail> getChatSessionDetail(int chatSessionId) async {
    return await _chatDetailService.fetchChatSessionDetail(chatSessionId);
  }
}
