import 'package:flutter/material.dart';
import 'package:flutterholmuskapp/home/main.dart';
import 'package:flutterholmuskapp/login/loginViewModel.dart';
import 'package:flutterholmuskapp/repository/user/userRepository.dart';

class LoginPage extends StatefulWidget {
  @override
  LoginPageState createState() => LoginPageState();
}

class LoginPageState extends State<LoginPage> {
  final LoginViewModel _loginViewModel = new LoginViewModel(UserRepository(App.localStorage));

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // Add 6 lines from here...
      appBar: AppBar(
        title: Text('Login'),
      ),
      body: Center(
        child: RaisedButton(
          child: Text('Login / Register'),
          onPressed: () {
            _loginViewModel.login("Holmusk User");
            Navigator.pop(context, true);
          },
        ),
      ),
    );
  }
}
