import 'package:flutterholmuskapp/repository/user/userRepository.dart';

class LoginViewModel {
  final IUserRepository _userRepository;

  LoginViewModel(this._userRepository);

  void login(String user) => _userRepository.login(user);

  bool isLoggedIn() => _userRepository.isLoggedIn();

  void logout() => _userRepository.logout();

  String getLoggedInUser() => _userRepository.getLoggedInUser();
}
