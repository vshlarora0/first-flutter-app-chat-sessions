import 'package:flutter/material.dart';
import 'package:flutterholmuskapp/chat/chatSessionsPage.dart';
import 'package:flutterholmuskapp/login/loginPage.dart';
import 'package:flutterholmuskapp/login/loginViewModel.dart';
import 'package:flutterholmuskapp/repository/user/userRepository.dart';
import 'package:shared_preferences/shared_preferences.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await App.init();
  runApp(MainApp());
}

class App {
  static SharedPreferences localStorage;

  static Future init() async {
    localStorage = await SharedPreferences.getInstance();
  }
}

class MainApp extends StatelessWidget {
  MainApp();

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Holmusk Application',
      home: HomePage(),
      theme: ThemeData(
        // Add the 3 lines from here...
        primaryColor: Colors.black,
      ),
    );
  }
}

class HomePage extends StatefulWidget {
  @override
  HomePageState createState() => HomePageState();
}

class HomePageState extends State<HomePage> {
  final LoginViewModel _loginViewModel = LoginViewModel(UserRepository(App.localStorage));

  @override
  Widget build(BuildContext context) {
    final _userLoggedIn = _loginViewModel.isLoggedIn();
    String greeting = _userLoggedIn
        ? 'Hello ${_loginViewModel.getLoggedInUser()}'
        : 'Hello Anonymous';
    return Scaffold(
      // Add 6 lines from here...
      appBar: AppBar(
        title: Text('Home'),
      ),
      body: Center(
        child: RaisedButton(
          child: Text(greeting),
          onPressed: () {
            if (_userLoggedIn) {
              _openChats(context);
            } else {
              _loginAndOpenChats(context);
            }
          },
        ),
      ),
    );
  }

  _loginAndOpenChats(BuildContext context) async {
    final _loggedIn = await Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => LoginPage()),
    );

    if (_loggedIn) {
      _openChats(context);
    }
  }

  _openChats(BuildContext context) {
    Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => ChatSessionsPage(),
        ));
  }
}
